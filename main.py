#!/usr/bin/env python2
import sys

multiplier = 0x5DEECE66D
addend     = 0xB
mask       = (1 << 48) - 1

def getSeed(a, b):
    for i in range(65536):
        seed = (a << 16) + i
        if (((seed * multiplier + addend) & mask) >> 16) == b:
            return seed
    return None

a = -1081936240 #
b = 1171948872  # Generated by 1 java.util.Random(0xdeadL)
c = 1520157765  #

print(getSeed(a, b))
print(getSeed(b, c))

# this shit doesn't seems to work
